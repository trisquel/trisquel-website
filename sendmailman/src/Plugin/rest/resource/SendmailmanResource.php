<?php

#    Copyright (C) 2023  Rubén Rodríguez <ruben@trisquel.org>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

namespace Drupal\sendmailman\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Database\Connection;

/**
 * Provides a Sendmailman Resource
 *
 * @RestResource(
 *   id = "sendmailman",
 *   label = @Translation("Sendmailman"),
 *   uri_paths = {
 *     "canonical" = "/sendmailman/{field}/{value}",
 *     "create" = "/sendmailman"
 *   }
 * )
 */
class SendmailmanResource extends ResourceBase {

  /**
   * Responds to entity GET requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function get($field, $value) {
    $query = \Drupal::database()->select('sendmailman')
             ->fields('sendmailman', ['lid','msgid','nid','cid','pid','uid','mid','tid'])
             ->condition($field, $value, '=')
	     ->orderBy('lid', 'DESC')
             ->execute();
    $results = $query->fetchAll();
    // Convert the results to an array.
    $data = [];
    foreach ($results as $result) {
      $data[] = [
      'lid' => $result->lid,
      'msgid' => $result->msgid,
      'nid' => $result->nid,
      'cid' => $result->cid,
      'pid' => $result->pid,
      'uid' => $result->uid,
      'mid' => $result->mid,
      'tid' => $result->tid,
      ];
      }
    return new ResourceResponse($data);
  }
   /**
   * Responds to entity POST requests.
   * @param $data
   * @return \Drupal\rest\ResourceResponse
   */
  public function POST($data) {
    try{
      $insert = \Drupal::database()->insert('sendmailman')
        ->fields($data)
	->execute();
      if ($insert) {
        return new ResourceResponse($insert);
      }
      else {
        return new ResourceResponse("error");
      }
    }
    catch (\Drupal\Core\Database\IntegrityConstraintViolationException $e) {
        return new ResourceResponse("IntegrityConstraintViolationException");
    }
  }
}

