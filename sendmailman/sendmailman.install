<?php

#    Copyright (C) 2023  Rubén Rodríguez <ruben@trisquel.org>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA


/**
 * @file
 * Install, update and uninstall functions for the sendmailman module.
 */

/**
 * Implements hook_install().
 */
function sendmailman_install() {
  \Drupal::messenger()->addStatus(__FUNCTION__);
}

/**
 * Implements hook_uninstall().
 */
function sendmailman_uninstall() {
  \Drupal::messenger()->addStatus(__FUNCTION__);
}

/**
 * Implements hook_schema().
 */
function sendmailman_schema() {
  $schema['sendmailman'] = [
    'description' => 'Table description.',
    'fields' => [
      'lid' => [
        'type' => 'serial',
        'not null' => TRUE,
      ],
      'msgid' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '0',
      ],
      'nid' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'cid' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'pid' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'uid' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'mid' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'tid' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
    ],
    'primary key' => ['lid'],
    'indexes' => [
      'nid' => ['nid'],
      'cid' => ['cid'],
      'pid' => ['pid'],
      'uid' => ['uid'],
      'mid' => ['mid'],
      'tid' => ['tid'],
    ],
  ];

  return $schema;
}

/**
 * Implements hook_requirements().
 */
function sendmailman_requirements($phase) {
  $requirements = [];

  if ($phase == 'runtime') {
    $value = mt_rand(0, 100);
    $requirements['sendmailman_status'] = [
      'title' => t('sendmailman status'),
      'value' => t('sendmailman value: @value', ['@value' => $value]),
      'severity' => $value > 50 ? REQUIREMENT_INFO : REQUIREMENT_WARNING,
    ];
  }

  return $requirements;
}
