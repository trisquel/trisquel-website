# trisquel-website

Base project to report and follow up on trisquel.info development.

## Subprojects

 * [SendDrupal](senddrupal): A simple mail client that sends mailing list subscriptions to Drupal forums.
 * [SendMailman](sendmailman): A Drupal 9 module that sends new forum nodes/comments to a mailing list
 * [D6-D9-migration](D6-D9-migration): Tools and documentation for importing the legacy Drupal 6 site into a new Drupal 9 installation
 * [D9-theme](D9-theme): A custom theme for the trisquel.org site
